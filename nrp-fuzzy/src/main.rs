use std::collections::HashMap;

use fuzzball::{
    solver::Solver,
    util::WriteToImage,
};

fn user_mode() -> Result<(), String> {
    let mut args = std::env::args().skip(1);
    let rules = args.next()
        .ok_or(String::from("Missing first argument: rules!"))?;
    let solver = Solver::from_file(rules)
        .map_err(|err| format!("Error creating solver: {}", err))?;

    let mut save_solver = false;
    let mut save_io = false;
    let mut no_run = false;
    let mut inputs = HashMap::new();
    for input in args {
        if input == "save-solver" {
            save_solver = true;
            continue;
        }
        if input == "save-io" {
            save_io = true;
            continue;
        }
        if input == "no-run" {
            no_run = true;
            continue;
        }
        let mut input = input.split_terminator('=');
        let name = input.next()
            .ok_or(String::from("Inputs must be formatted like 'input=value', with no spaces!"))?;
        let value = input.next()
            .ok_or(format!("Inputs must be formatted like 'input=value', with no spaces!"))?;
        let value = value.parse::<f64>()
            .map_err(|_| format!("Could not parse input value '{}'!", value))?;
        inputs.insert(name.to_string(), value);
    }

    if save_io {
        for (name, input) in solver.inputs() {
            let dir = format!("out/inputs/{}/", name);
            for (name, func) in input.sets() {
                let dir = format!("{}{}.png", dir, name);
                func.save_to_image(dir, 512, 256, Default::default());
            }
        }

        for (name, input) in solver.outputs() {
            let dir = format!("out/outputs/{}/", name);
            for (name, set) in input.sets() {
                let dir = format!("{}{}.png", dir, name);
                let func = set.membership_function();
                func.save_to_image(dir, 512, 256, Default::default());
            }
        }
    }

    if !no_run {
        let result = solver.run(inputs, save_solver)
            .map_err(|err| format!("Error running solver: {}", err))?;

        println!("{} result set(s):", result.len());
        for (name, set) in result {
            println!("> {} IS {}", name, set);
        }
    }

    Ok(())
}

fn main() {
    if let Err(err) = user_mode() {
        println!("{}", err);
    }
}