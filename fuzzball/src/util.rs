use std::ops::{Sub, Div};

#[cfg(feature = "to-image")]
use image::{DynamicImage, Rgba};


pub fn clamp<T: PartialOrd>(val: T, min: T, max: T) -> T {
    assert!(min <= max, "Min must be less than or equal to max!");
    if val < min { min } else if val > max { max } else { val }
}


pub fn fuzzy_xor(a: f64, b: f64) -> f64 {
    let mul = a * b;
    let mul_inv = (1.0 - a) * (1.0 - b);
    1.0 - f64::max(mul, mul_inv)
}


pub fn normalize<T>(min: T, max: T, val: T) -> T
    where T: Copy + Sub<T, Output=T> + Div<T, Output=T> {
    (val - min) / (max - min)
}


pub trait WriteToBuffer<T> {
    fn write_to_buffer(&self, buffer: &mut [T]);
}


#[cfg(feature = "to-image")]
pub struct ImageStyle {
    pub above_color: Rgba<u8>,
    pub line_color: Rgba<u8>,
    pub below_color: Rgba<u8>,
    pub line_thickness: f64,
}

#[cfg(feature = "to-image")]
impl Default for ImageStyle {
    fn default() -> Self {
        ImageStyle {
            above_color: Rgba([255, 255, 255, 255]),
            line_color: Rgba([95, 95, 95, 255]),
            below_color: Rgba([191, 191, 191, 255]),
            line_thickness: 1.0,
        }
    }
}


#[cfg(feature = "to-image")]
pub trait WriteToImage {
    fn write_to_image(&self, image: &mut DynamicImage, style: &ImageStyle);

    fn save_to_image(&self,
                     path: impl AsRef<std::path::Path>,
                     width: u32,
                     height: u32,
                     style: ImageStyle) {
        let mut image = DynamicImage::new_rgb8(width, height);
        self.write_to_image(&mut image, &style);
        use std::fs;
        use std::path::Path;
        if let Some(dir) = Path::parent(path.as_ref()) {
            let _ = fs::create_dir_all(dir);
            let _ = image.save(path.as_ref());
        }
    }
}

#[cfg(feature = "to-image")]
fn buffer_to_image(buffer: &[f64], image: &mut DynamicImage, style: &ImageStyle) {
    use image::{GenericImage, GenericImageView};
    let (width, height) = image.dimensions();
    for x in 0..width {
        let mapped = (1.0 - buffer[x as usize]) * (height - 1) as f64;
        for y in 0..height {
            let diff = y as f64 - mapped;
            let color = if f64::abs(diff) < style.line_thickness {
                style.line_color
            } else if diff < 0.0 {
                style.above_color
            } else {
                style.below_color
            };
            unsafe { image.unsafe_put_pixel(x, y, color); }
        }
    }
}

#[cfg(feature = "to-image")]
impl<T: WriteToBuffer<f64>> WriteToImage for T {
    fn write_to_image(&self, image: &mut DynamicImage, style: &ImageStyle) {
        use image::GenericImageView;
        let (width, _) = image.dimensions();
        let mut buffer = vec![0 as f64; width as usize];
        self.write_to_buffer(&mut buffer);
        buffer_to_image(&buffer, image, style);
    }
}
