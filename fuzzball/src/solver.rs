use std::collections::HashMap;
use std::path::Path;
use std::fs;

use crate::error::{Error, Result};
use crate::rule::Context;
use crate::variable::{Input, Output};
use crate::parser;


pub struct Solver {
    inputs: HashMap<String, Input>,
    outputs: HashMap<String, Output>,
}

impl Solver {
    pub fn new(inputs: HashMap<String, Input>, outputs: HashMap<String, Output>) -> Solver {
        Solver { inputs, outputs }
    }

    pub fn from_file(path: impl AsRef<Path>) -> Result<Solver> {
        let data = fs::read_to_string(path)
            .map_err(|err| Error::FileError(err.to_string()))?;
        Ok(parser::fz_to_slover(&data)?)
    }

    pub fn inputs(&self) -> &HashMap<String, Input> {
        &self.inputs
    }

    pub fn outputs(&self) -> &HashMap<String, Output> {
        &self.outputs
    }

    pub fn run(&self,
               params: HashMap<String, f64>,
               save_images: bool)
               -> Result<HashMap<String, String>> {
        let context = Context::new(&params, &self.inputs);
        self.outputs.iter()
            .map(|(name, output)| {
                let dir = if save_images { Some(format!("out/solver/{}/", name)) } else { None };
                match output.resolve(&context, dir) {
                    Ok(result) => Ok((name.clone(), result)),
                    Err(err) => Err(err)
                }
            })
            .collect()
    }
}
