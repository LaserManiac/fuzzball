use std::collections::HashMap;
use std::mem;
use std::fmt::{Display, Formatter, Result as FmtResult};

use crate::error::{Error, Result};
use crate::variable::Input;


pub struct Context<'a> {
    params: &'a HashMap<String, f64>,
    sets: &'a HashMap<String, Input>,
}

impl<'a> Context<'a> {
    pub fn new(params: &'a HashMap<String, f64>, sets: &'a HashMap<String, Input>) -> Self {
        Context { params, sets }
    }

    pub fn evaluate(&self, name: &str, set: &str) -> Result<f64> {
        let input = self.sets.get(name)
            .ok_or_else(|| Error::MissingInputVariable(name.into()))?;
        let value = *self.params.get(name)
            .ok_or_else(|| Error::MissingInputParameter(name.into()))?;
        input.compute(set, value)
    }
}


#[derive(Clone)]
pub enum Rule {
    Is(String, String),
    Not(Box<Rule>),
    Or(Box<Rule>, Box<Rule>),
    And(Box<Rule>, Box<Rule>),
    Xor(Box<Rule>, Box<Rule>),
}

impl Rule {
    pub fn is(name: impl Into<String>, descr: impl Into<String>) -> Self {
        Rule::Is(name.into(), descr.into())
    }

    pub fn not(self) -> Self {
        Rule::Not(Box::new(self))
    }

    pub fn or(self, other: Rule) -> Self {
        Rule::Or(Box::new(self), Box::new(other))
    }

    pub fn and(self, other: Rule) -> Self {
        Rule::And(Box::new(self), Box::new(other))
    }

    pub fn xor(self, other: Rule) -> Self {
        Rule::Xor(Box::new(self), Box::new(other))
    }

    pub fn set_not(&mut self) {
        unsafe {
            let old = mem::replace(self, mem::uninitialized());
            *self = Rule::Not(Box::new(old))
        }
    }

    pub fn set_or(&mut self, other: Rule) {
        unsafe {
            let old = mem::replace(self, mem::uninitialized());
            *self = Rule::Or(Box::new(old), Box::new(other))
        }
    }

    pub fn set_and(&mut self, other: Rule) {
        unsafe {
            let old = mem::replace(self, mem::uninitialized());
            *self = Rule::And(Box::new(old), Box::new(other))
        }
    }

    pub fn set_xor(&mut self, other: Rule) {
        unsafe {
            let old = mem::replace(self, mem::uninitialized());
            *self = Rule::Xor(Box::new(old), Box::new(other))
        }
    }

    pub fn is_base(&self) -> bool {
        match self {
            Rule::Is(..) => true,
            _ => false
        }
    }

    pub fn evaluate(&self,
                    context: &Context)
                    -> Result<f64> {
        match self {
            Rule::Is(name, descr) => {
                context.evaluate(name, descr)
            }
            Rule::Not(xpr) => {
                let xpr = xpr.evaluate(context)?;
                Ok(1.0 - xpr)
            }
            Rule::Or(first, second) => {
                let first = first.evaluate(context)?;
                let second = second.evaluate(context)?;
                Ok(f64::max(first, second))
            }
            Rule::And(first, second) => {
                let first = first.evaluate(context)?;
                let second = second.evaluate(context)?;
                Ok(f64::min(first, second))
            }
            Rule::Xor(first, second) => {
                let first = first.evaluate(context)?;
                let second = second.evaluate(context)?;
                Ok(crate::util::fuzzy_xor(first, second))
            }
        }
    }
}

impl Display for Rule {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        match self {
            Rule::Not(xpr) if xpr.is_base() => {
                match &**xpr {
                    Rule::Is(name, descr) =>
                        f.write_fmt(format_args!("{} IS NOT {}", name, descr)),
                    _ => Ok(())
                }
            }
            Rule::Is(name, descr) =>
                f.write_fmt(format_args!("{} IS {}", name, descr)),
            Rule::Not(xpr) =>
                f.write_fmt(format_args!("NOT {}", xpr)),
            Rule::Or(first, second) =>
                f.write_fmt(format_args!("({} OR {})", first, second)),
            Rule::And(first, second) =>
                f.write_fmt(format_args!("({} AND {})", first, second)),
            Rule::Xor(first, second) =>
                f.write_fmt(format_args!("({} XOR {})", first, second))
        }
    }
}
