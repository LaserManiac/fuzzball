use std::fmt::{Display, Formatter, Result as FmtResult};

use crate::parser::Error as ParseError;


#[derive(Debug)]
pub enum Error {
    MissingInputVariable(String),
    MissingInputParameter(String),
    MissingSet(String),
    DuplicateSetName(String),
    ValueOutOfRange(f64, f64, f64),
    FileError(String),
    ParserError(ParseError),
}

impl From<ParseError> for Error {
    fn from(err: ParseError) -> Self {
        Error::ParserError(err)
    }
}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        match self {
            Error::MissingInputVariable(var) =>
                f.write_fmt(format_args!("Missing input variable {}!", var)),
            Error::MissingInputParameter(par) =>
                f.write_fmt(format_args!("Missing input parameter {}!", par)),
            Error::MissingSet(set) =>
                f.write_fmt(format_args!("Missing fuzzy set {}!", set)),
            Error::DuplicateSetName(var) =>
                f.write_fmt(format_args!("Duplicate fuzzy set name {}!", var)),
            Error::ValueOutOfRange(min, max, val) =>
                f.write_fmt(format_args!("Value {} is out of range {}-{}!", val, min, max)),
            Error::FileError(msg) =>
                f.write_fmt(format_args!("File error: {}", msg)),
            Error::ParserError(err) =>
                f.write_fmt(format_args!("Parser error: {}", err))
        }
    }
}

pub type Result<T> = std::result::Result<T, Error>;