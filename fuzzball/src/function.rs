use crate::util::WriteToBuffer;


#[derive(Debug, Clone)]
pub struct Params {
    pub hshift: f64,
    pub vshift: f64,
    pub hscale: f64,
    pub vscale: f64,
    pub cutoff: f64,
}

impl Params {
    pub fn basic(center: f64, width: f64, height: f64) -> Self {
        Params {
            hshift: center,
            hscale: width,
            vscale: height,
            ..Default::default()
        }
    }

    pub fn trapezoid(center: f64, bottom_width: f64, top_width: f64, height: f64) -> Self {
        Params {
            hshift: center,
            hscale: bottom_width,
            vscale: 1.0 / (1.0 - top_width / bottom_width) * height,
            cutoff: height,
            ..Default::default()
        }
    }
}

impl Default for Params {
    fn default() -> Self {
        Params {
            hshift: 0.0,
            vshift: 0.0,
            hscale: 1.0,
            vscale: 1.0,
            cutoff: 1.0,
        }
    }
}


#[derive(Debug, Clone)]
pub enum MembershipFunction {
    Sine(Params),
    Triangle(Params),
}

impl MembershipFunction {
    pub fn get_fuzzy(&self, input: f64) -> f64 {
        match self {
            MembershipFunction::Sine(params) => {
                let input = (input - params.hshift) / params.hscale + 0.5;
                let input = if input >= 0.0 && input <= 1.0 {
                    let angle = (input * 2.0 - 0.5) * std::f64::consts::PI;
                    (f64::sin(angle) + 1.0) / 2.0
                } else {
                    0.0
                };
                let input = input * params.vscale + params.vshift;
                f64::min(input, params.cutoff)
            }
            MembershipFunction::Triangle(params) => {
                let input = (input - params.hshift) / params.hscale + 0.5;
                let input = if input >= 0.0 && input <= 1.0 {
                    if input < 0.5 {
                        input / 0.5
                    } else {
                        (1.0 - input) / 0.5
                    }
                } else {
                    0.0
                };
                let input = input * params.vscale + params.vshift;
                f64::min(input, params.cutoff)
            }
        }
    }
}

impl WriteToBuffer<f64> for MembershipFunction {
    fn write_to_buffer(&self, buffer: &mut [f64]) {
        let len = (buffer.len() - 1) as f64;
        for (idx, val) in buffer.iter_mut().enumerate() {
            let value = idx as f64 / len;
            *val = self.get_fuzzy(value);
        }
    }
}
