use std::collections::HashMap;

use crate::error::{Error, Result};
use crate::util::WriteToBuffer;
use crate::function::MembershipFunction;
use crate::rule::{Rule, Context};

pub struct Input {
    min: f64,
    max: f64,
    sets: HashMap<String, MembershipFunction>,
}

impl Input {
    pub fn new(min: f64, max: f64) -> Self {
        Input { min, max, sets: HashMap::new() }
    }

    pub fn add_set(&mut self,
                   name: impl Into<String>,
                   function: MembershipFunction)
                   -> Result<()> {
        let name = name.into();
        if self.sets.contains_key(&name) {
            Err(Error::DuplicateSetName(name))
        } else {
            self.sets.insert(name, function);
            Ok(())
        }
    }

    pub fn sets(&self) -> &HashMap<String, MembershipFunction> {
        &self.sets
    }

    pub fn compute(&self, name: &str, value: f64) -> Result<f64> {
        if value < self.min || value > self.max {
            return Err(Error::ValueOutOfRange(self.min, self.max, value));
        }
        let func = self.sets.get(name)
            .ok_or_else(|| Error::MissingSet(name.into()))?;
        let value = crate::util::normalize(self.min, self.max, value);
        Ok(func.get_fuzzy(value))
    }
}


#[derive(Debug, Copy, Clone)]
pub enum ImplicationMethod {
    Min,
    Prod,
}

impl ImplicationMethod {
    pub fn resolve(&self, implicator: f64, implicate: f64) -> f64 {
        match self {
            ImplicationMethod::Min => f64::min(implicator, implicate),
            ImplicationMethod::Prod => implicator * implicate,
        }
    }
}


pub struct Implicate {
    method: ImplicationMethod,
    implicate: MembershipFunction,
    implicator: f64,
}

impl Implicate {
    pub fn get_fuzzy(&self, value: f64) -> f64 {
        self.method.resolve(self.implicator, self.implicate.get_fuzzy(value))
    }
}

impl WriteToBuffer<f64> for Implicate {
    fn write_to_buffer(&self, buffer: &mut [f64]) {
        let len = (buffer.len() - 1) as f64;
        for (idx, val) in buffer.iter_mut().enumerate() {
            let value = idx as f64 / len;
            *val = self.get_fuzzy(value);
        }
    }
}


pub struct OutputSet {
    func: MembershipFunction,
    rule: Rule,
    method: ImplicationMethod,
}

impl OutputSet {
    pub fn new(func: MembershipFunction, rule: Rule, method: ImplicationMethod) -> Self {
        OutputSet { func, rule, method }
    }

    pub fn membership_function(&self) -> &MembershipFunction {
        &self.func
    }

    pub fn get_implicate(&self, context: &Context) -> Result<Implicate> {
        let implicator = self.rule.evaluate(context)?;
        Ok(Implicate { implicator, implicate: self.func.clone(), method: self.method })
    }
}


#[derive(Debug, Copy, Clone)]
pub enum AggregationMethod {
    Max,
    Probor,
    Sum,
}

impl AggregationMethod {
    pub fn resolve(&self, a: f64, b: f64) -> f64 {
        match self {
            AggregationMethod::Max => f64::max(a, b),
            AggregationMethod::Sum => a + b,
            AggregationMethod::Probor => a + b - a * b,
        }
    }
}


pub struct Aggregate<'a> {
    method: AggregationMethod,
    aggregatees: Vec<&'a Implicate>,
}

impl<'a> Aggregate<'a> {
    pub fn new(method: AggregationMethod, aggregatees: Vec<&'a Implicate>) -> Self {
        assert!(aggregatees.len() > 0, "An aggregate must have at least one aggregatee!");
        Aggregate::<'a> { method, aggregatees }
    }

    pub fn get_fuzzy(&self, value: f64) -> f64 {
        let mut aggregate = self.aggregatees.first()
            .unwrap()
            .get_fuzzy(value);
        for aggregatee in &self.aggregatees[1..] {
            let aggregatee = aggregatee.get_fuzzy(value);
            aggregate = self.method.resolve(aggregate, aggregatee);
        }
        aggregate
    }
}

impl<'a> WriteToBuffer<f64> for Aggregate<'a> {
    fn write_to_buffer(&self, buffer: &mut [f64]) {
        let len = (buffer.len() - 1) as f64;
        for (idx, val) in buffer.iter_mut().enumerate() {
            let value = idx as f64 / len;
            *val = self.get_fuzzy(value);
        }
    }
}


#[derive(Debug, Copy, Clone)]
pub enum DefuzzificationMethod {
    Centroid,
    Som,
    Mom,
    Lom,
}

impl DefuzzificationMethod {
    pub fn defuzzify(&self, data: &[f64]) -> f64 {
        match self {
            DefuzzificationMethod::Centroid => {
                let mut c = 0.0;
                let mut a = 0.0;
                for (i, v) in data.iter().enumerate() {
                    a += v;
                    c += v * i as f64;
                }
                c / data.len() as f64 / a
            }
            DefuzzificationMethod::Som => {
                let mut max = 0;
                for i in 1..data.len() {
                    if data[i] > data[max] {
                        max = i;
                    }
                }
                max as f64 / data.len() as f64
            }
            DefuzzificationMethod::Mom => {
                let mut max_first = 0;
                let mut max_second = 0;
                for i in 1..data.len() {
                    if data[i] > data[max_first] {
                        max_first = i;
                    }
                    if data[i] >= data[max_second] {
                        max_second = i;
                    }
                }
                (max_first + max_second) as f64 / 2.0 / data.len() as f64
            }
            DefuzzificationMethod::Lom => {
                let mut max = 0;
                for i in 1..data.len() {
                    if data[i] >= data[max] {
                        max = i;
                    }
                }
                max as f64 / data.len() as f64
            }
        }
    }
}


pub struct Output {
    #[allow(unused)]
    min: f64,
    #[allow(unused)]
    max: f64,
    agg_method: AggregationMethod,
    def_method: DefuzzificationMethod,
    sets: HashMap<String, OutputSet>,
}

impl Output {
    pub fn new(min: f64,
               max: f64,
               agg_method: AggregationMethod,
               def_method: DefuzzificationMethod)
               -> Self {
        Output { min, max, agg_method, def_method, sets: HashMap::new() }
    }

    pub fn add_set(&mut self,
                   name: impl Into<String>,
                   set: OutputSet)
                   -> Result<()> {
        let name = name.into();
        if self.sets.contains_key(&name) {
            Err(Error::DuplicateSetName(name))
        } else {
            self.sets.insert(name, set);
            Ok(())
        }
    }

    pub fn sets(&self) -> &HashMap<String, OutputSet> {
        &self.sets
    }

    pub fn resolve(&self, context: &Context, image_folder: Option<String>) -> Result<String> {
        let mut implicates = Vec::new();
        for (name, set) in &self.sets {
            let implicate = set.get_implicate(context)?;
            implicates.push((name, implicate));
        }

        let aggregatees = implicates.iter()
            .map(|(_, implicate)| implicate)
            .collect();
        let aggregate = Aggregate::new(self.agg_method, aggregatees);

        if let Some(folder) = image_folder {
            use crate::util::WriteToImage;
            for (name, implicate) in &implicates {
                let path = format!("{}implicate-{}.png", folder, name);
                implicate.save_to_image(path, 512, 256, Default::default());
            }
            let path = format!("{}aggregate.png", folder);
            aggregate.save_to_image(path, 512, 256, Default::default());
        }

        let mut buffer = vec![0 as f64; 1000];
        aggregate.write_to_buffer(&mut buffer);
        let result = self.def_method.defuzzify(&buffer);

        let mut best_idx = 0;
        let mut best_val = implicates[0].1.get_fuzzy(result);
        for (idx, (_, implicate)) in implicates.iter().enumerate().skip(1) {
            let val = implicate.get_fuzzy(result);
            if val > best_val {
                best_val = val;
                best_idx = idx;
            }
        }

        Ok(implicates[best_idx].0.clone())
    }
}
