use std::fmt::{Display, Formatter, Result as FmtResult};
use std::collections::HashMap;

use crate::solver::Solver;
use crate::function::{MembershipFunction, Params};
use crate::rule::Rule;
use crate::variable::{Input, Output, OutputSet, ImplicationMethod, AggregationMethod, DefuzzificationMethod};


#[derive(Debug)]
pub enum Error {
    InvalidCharacter(Location, String),
    InvalidNumber(Location, String),
    InvalidToken(Location, String),
    UnexpectedEnd(String),
    WrongNumberOfArguments(Location),
    UndeclaredVariable(Location, String),
    DuplicateSetName(Location, String),
}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        match self {
            Error::InvalidCharacter(loc, hint) =>
                f.write_fmt(format_args!("Invalid character on {}: {}", loc, hint)),
            Error::InvalidNumber(loc, hint) =>
                f.write_fmt(format_args!("Could not parse number on {}: {}", loc, hint)),
            Error::InvalidToken(loc, hint) =>
                f.write_fmt(format_args!("Invalid token on {}: {}", loc, hint)),
            Error::UnexpectedEnd(hint) =>
                f.write_fmt(format_args!("Unexpected end of file: {}", hint)),
            Error::WrongNumberOfArguments(loc) =>
                f.write_fmt(format_args!("Wrong number of arguments on {}!", loc)),
            Error::UndeclaredVariable(loc, name) =>
                f.write_fmt(format_args!("Referencing undeclared variable {} on {}!", name, loc)),
            Error::DuplicateSetName(loc, name) =>
                f.write_fmt(format_args!("Duplicate set name '{}' on {}", name, loc))
        }
    }
}

type Result<T> = std::result::Result<T, Error>;


#[derive(PartialEq, Copy, Clone)]
enum Keyword {
    Is,
    Not,
    For,
    When,
    With,
    Between,
    Implied,
    Aggregated,
    Min,
    Max,
    And,
    Or,
    Xor,
    Prod,
    Sum,
    Probor,
    Triangle,
    Trapezoid,
    Sine,
    Defuzzified,
    Centroid,
    Som,
    Mom,
    Lom,
}

impl Display for Keyword {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        match self {
            Keyword::Is => f.write_str("IS"),
            Keyword::Not => f.write_str("NOT"),
            Keyword::For => f.write_str("FOR"),
            Keyword::When => f.write_str("WHEN"),
            Keyword::With => f.write_str("WITH"),
            Keyword::Between => f.write_str("BETWEEN"),
            Keyword::Implied => f.write_str("IMPLIED"),
            Keyword::Aggregated => f.write_str("AGGREGATED"),
            Keyword::Min => f.write_str("MIN"),
            Keyword::Max => f.write_str("MAX"),
            Keyword::And => f.write_str("AND"),
            Keyword::Or => f.write_str("OR"),
            Keyword::Xor => f.write_str("XOR"),
            Keyword::Prod => f.write_str("PROD"),
            Keyword::Sum => f.write_str("SUM"),
            Keyword::Probor => f.write_str("PROBOR"),
            Keyword::Triangle => f.write_str("TRIANGLE"),
            Keyword::Trapezoid => f.write_str("TRAPEZOID"),
            Keyword::Sine => f.write_str("SINE"),
            Keyword::Defuzzified => f.write_str("DEFUZZIFIED"),
            Keyword::Centroid => f.write_str("CENTROID"),
            Keyword::Som => f.write_str("SOM"),
            Keyword::Mom => f.write_str("MOM"),
            Keyword::Lom => f.write_str("LOM"),
        }
    }
}

impl Keyword {
    fn get_keyword(word: &str) -> Option<Keyword> {
        let word = word.to_uppercase();
        match word.as_str() {
            "IS" => Some(Keyword::Is),
            "NOT" => Some(Keyword::Not),
            "FOR" => Some(Keyword::For),
            "WHEN" => Some(Keyword::When),
            "WITH" => Some(Keyword::With),
            "BETWEEN" => Some(Keyword::Between),
            "IMPLIED" => Some(Keyword::Implied),
            "AGGREGATED" => Some(Keyword::Aggregated),
            "MIN" => Some(Keyword::Min),
            "MAX" => Some(Keyword::Max),
            "AND" => Some(Keyword::And),
            "OR" => Some(Keyword::Or),
            "XOR" => Some(Keyword::Xor),
            "PROD" => Some(Keyword::Prod),
            "SUM" => Some(Keyword::Sum),
            "PROBOR" => Some(Keyword::Probor),
            "TRIANGLE" => Some(Keyword::Triangle),
            "TRAPEZOID" => Some(Keyword::Trapezoid),
            "SINE" => Some(Keyword::Sine),
            "DEFUZZIFIED" => Some(Keyword::Defuzzified),
            "CENTROID" => Some(Keyword::Centroid),
            "SOM" => Some(Keyword::Som),
            "MOM" => Some(Keyword::Mom),
            "LOM" => Some(Keyword::Lom),
            _ => None
        }
    }
}


#[derive(PartialEq)]
enum Token {
    Keyword(Keyword),
    Number(f64),
    Name(String),
    OpenParent,
    ClosedParent,
    Comma,
    Semicolon,
}

impl Display for Token {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        match self {
            Token::Keyword(keyword) => keyword.fmt(f),
            Token::Number(number) => number.fmt(f),
            Token::Name(name) => f.write_fmt(format_args!("'{}'", name)),
            Token::OpenParent => f.write_str("("),
            Token::ClosedParent => f.write_str(")"),
            Token::Comma => f.write_str(","),
            Token::Semicolon => f.write_str(";"),
        }
    }
}


#[derive(Debug, Copy, Clone)]
pub struct Location {
    line: usize,
    index: usize,
}

impl Location {
    pub fn new(line: usize, index: usize) -> Self {
        Location { line, index }
    }
}

impl Display for Location {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        f.write_fmt(format_args!("line {} char {}", self.line, self.index))
    }
}


struct ParsedToken(Location, Token);


struct TokenStream {
    tokens: Vec<ParsedToken>,
    index: usize,
}

impl TokenStream {
    fn new(tokens: Vec<ParsedToken>) -> Self {
        TokenStream { tokens, index: 0 }
    }

    fn empty(&self) -> bool {
        self.index >= self.tokens.len()
    }

    fn next(&mut self) -> Option<&ParsedToken> {
        self.index += 1;
        self.tokens.get(self.index - 1)
    }

    fn back(&mut self) {
        self.index -= 1;
    }

    fn location(&self) -> Location {
        self.tokens[self.index].0
    }

    fn consume_keyword(&mut self, keyword: Keyword) -> Result<()> {
        match self.next() {
            Some(token) => {
                match &token.1 {
                    Token::Keyword(kw) if *kw == keyword => Ok(()),
                    _ => {
                        let location = token.0;
                        let hint = format!("Expected {}, got {}!", keyword, token.1);
                        self.back();
                        Err(Error::InvalidToken(location, hint))
                    }
                }
            }
            None => {
                let hint = format!("Expected {}!", keyword);
                Err(Error::UnexpectedEnd(hint))
            }
        }
    }

    fn consume_name(&mut self) -> Result<String> {
        match self.next() {
            Some(token) => {
                match &token.1 {
                    Token::Name(name) => Ok(name.clone()),
                    _ => {
                        let location = token.0;
                        let hint = format!("Expected name, got {}!", token.1);
                        self.back();
                        Err(Error::InvalidToken(location, hint))
                    }
                }
            }
            None => {
                let hint = format!("Expected name!");
                Err(Error::UnexpectedEnd(hint))
            }
        }
    }

    fn consume_number(&mut self) -> Result<f64> {
        match self.next() {
            Some(token) => {
                match &token.1 {
                    Token::Number(number) => Ok(*number),
                    _ => {
                        let location = token.0;
                        let hint = format!("Expected number, got {}!", token.1);
                        self.back();
                        Err(Error::InvalidToken(location, hint))
                    }
                }
            }
            None => {
                let hint = format!("Expected number!");
                Err(Error::UnexpectedEnd(hint))
            }
        }
    }

    fn consume_token(&mut self, expected: Token) -> Result<()> {
        match self.next() {
            Some(token) => {
                match &token.1 {
                    token if *token == expected => Ok(()),
                    _ => {
                        let location = token.0;
                        let hint = format!("Expected {}, got {}!", expected, token.1);
                        self.back();
                        Err(Error::InvalidToken(location, hint))
                    }
                }
            }
            None => {
                let hint = format!("Expected {}!", expected);
                Err(Error::UnexpectedEnd(hint))
            }
        }
    }

    fn consume_keyword_any(&mut self, keywords: &[Keyword]) -> Result<Keyword> {
        let expected = keywords.iter()
            .map(Keyword::to_string)
            .collect::<Vec<_>>()
            .join(", ");

        match self.next() {
            Some(token) => {
                match &token.1 {
                    Token::Keyword(kw) if keywords.contains(kw) => Ok(*kw),
                    _ => {
                        let location = token.0;
                        let hint = format!("Expected {}, got {}!", expected, token.1);
                        self.back();
                        Err(Error::InvalidToken(location, hint))
                    }
                }
            }
            None => {
                let hint = format!("Expected {}!", expected);
                Err(Error::UnexpectedEnd(hint))
            }
        }
    }

    fn consume_aggregation_method(&mut self) -> Result<AggregationMethod> {
        let agg_kws = &[Keyword::Max, Keyword::Sum, Keyword::Probor];
        match self.consume_keyword_any(agg_kws)? {
            Keyword::Max => Ok(AggregationMethod::Max),
            Keyword::Sum => Ok(AggregationMethod::Sum),
            Keyword::Probor => Ok(AggregationMethod::Probor),
            _ => unreachable!()
        }
    }

    fn consume_defuzzification_method(&mut self) -> Result<DefuzzificationMethod> {
        let def_kws = &[Keyword::Centroid, Keyword::Som, Keyword::Mom, Keyword::Lom];
        match self.consume_keyword_any(def_kws)? {
            Keyword::Centroid => Ok(DefuzzificationMethod::Centroid),
            Keyword::Som => Ok(DefuzzificationMethod::Som),
            Keyword::Mom => Ok(DefuzzificationMethod::Mom),
            Keyword::Lom => Ok(DefuzzificationMethod::Lom),
            _ => unreachable!()
        }
    }

    fn consume_implication_method(&mut self) -> Result<ImplicationMethod> {
        let imp_kws = &[Keyword::Min, Keyword::Prod];
        match self.consume_keyword_any(imp_kws)? {
            Keyword::Min => Ok(ImplicationMethod::Min),
            Keyword::Prod => Ok(ImplicationMethod::Prod),
            _ => unreachable!()
        }
    }

    fn consume_membership_function(&mut self) -> Result<MembershipFunction> {
        let mem_kws = &[Keyword::Triangle, Keyword::Trapezoid, Keyword::Sine];
        let keyword = self.consume_keyword_any(mem_kws)?;
        let location = self.location();
        let args = self.consume_number_list()?;
        match keyword {
            Keyword::Triangle => {
                if args.len() != 3 {
                    Err(Error::WrongNumberOfArguments(location))
                } else {
                    let params = Params::basic(args[0], args[1], args[2]);
                    Ok(MembershipFunction::Triangle(params))
                }
            }
            Keyword::Trapezoid => {
                if args.len() != 4 {
                    Err(Error::WrongNumberOfArguments(location))
                } else {
                    let params = Params::trapezoid(args[0], args[1], args[2], args[3]);
                    Ok(MembershipFunction::Triangle(params))
                }
            }
            Keyword::Sine => {
                if args.len() != 3 {
                    Err(Error::WrongNumberOfArguments(location))
                } else {
                    let params = Params::basic(args[0], args[1], args[2]);
                    Ok(MembershipFunction::Sine(params))
                }
            }
            _ => unreachable!()
        }
    }

    fn consume_number_list(&mut self) -> Result<Vec<f64>> {
        self.consume_token(Token::OpenParent)?;
        let mut list = Vec::new();
        if let Ok(_) = self.consume_token(Token::ClosedParent) {
            return Ok(list);
        } else if let Ok(number) = self.consume_number() {
            list.push(number);
            loop {
                if let Ok(_) = self.consume_token(Token::ClosedParent) {
                    return Ok(list);
                } else if let Ok(_) = self.consume_token(Token::Comma) {
                    let number = self.consume_number()?;
                    list.push(number);
                } else {
                    let expected = format!("Expected {} or {}", Token::ClosedParent, Token::Comma);
                    match self.next() {
                        Some(ParsedToken(location, token)) => {
                            let hint = format!("{}, got {}", expected, token);
                            return Err(Error::InvalidToken(*location, hint));
                        }
                        None => {
                            let hint = format!("{}!", expected);
                            return Err(Error::UnexpectedEnd(hint));
                        }
                    }
                }
            }
        }
        self.consume_token(Token::ClosedParent)?;
        Ok(list)
    }

    fn consume_rule_base(&mut self) -> Result<Rule> {
        let name = self.consume_name()?;
        self.consume_keyword(Keyword::Is)?;
        let mut not = false;
        if let Ok(_) = self.consume_keyword(Keyword::Not) {
            not = true;
        }
        let set = self.consume_name()?;
        let rule = if not {
            Rule::not(Rule::is(name, set))
        } else {
            Rule::is(name, set)
        };
        Ok(rule)
    }

    fn consume_rule_par(&mut self) -> Result<Rule> {
        let start = self.index;
        if let Ok(_) = self.consume_token(Token::OpenParent) {
            let rule = self.consume_rule_or()?;
            self.consume_token(Token::ClosedParent)?;
            Ok(rule)
        } else if let Ok(rule) = self.consume_rule_base() {
            Ok(rule)
        } else {
            let location = self.location();
            self.index = start;
            Err(Error::InvalidToken(location, String::from("Unexpected token in rule!")))
        }
    }

    fn consume_rule_and(&mut self) -> Result<Rule> {
        let rule = self.consume_rule_par()?;
        if let Ok(_) = self.consume_keyword(Keyword::And) {
            let other = self.consume_rule_and()?;
            Ok(Rule::And(Box::new(rule), Box::new(other)))
        } else {
            Ok(rule)
        }
    }

    fn consume_rule_xor(&mut self) -> Result<Rule> {
        let rule = self.consume_rule_and()?;
        if let Ok(_) = self.consume_keyword(Keyword::Xor) {
            let other = self.consume_rule_xor()?;
            Ok(Rule::Xor(Box::new(rule), Box::new(other)))
        } else {
            Ok(rule)
        }
    }

    fn consume_rule_or(&mut self) -> Result<Rule> {
        let rule = self.consume_rule_xor()?;
        if let Ok(_) = self.consume_keyword(Keyword::Or) {
            let other = self.consume_rule_or()?;
            Ok(Rule::Or(Box::new(rule), Box::new(other)))
        } else {
            Ok(rule)
        }
    }

    fn consume_rule(&mut self) -> Result<Rule> {
        self.consume_rule_or()
    }
}


fn tokenize(data: &str) -> Result<TokenStream> {
    enum Mode {
        None,
        Word,
        Number,
        Comment,
    }

    let mut tokens = Vec::new();
    let mut chars = data.chars().enumerate();
    let mut mode = Mode::None;
    let mut buffer = String::new();
    let mut line_start = 0;
    let mut line_idx = 1;
    let mut token_start = Location::new(line_idx, line_start);
    while let Some((i, c)) = chars.next() {
        let char_idx = i - line_start + 1;
        loop {
            match mode {
                Mode::None => {
                    if c == '(' {
                        tokens.push(ParsedToken(
                            Location::new(line_idx, char_idx),
                            Token::OpenParent,
                        ));
                    } else if c == ')' {
                        tokens.push(ParsedToken(
                            Location::new(line_idx, char_idx),
                            Token::ClosedParent,
                        ));
                    } else if c == ',' {
                        tokens.push(ParsedToken(
                            Location::new(line_idx, char_idx),
                            Token::Comma,
                        ));
                    } else if c == ';' {
                        tokens.push(ParsedToken(
                            Location::new(line_idx, char_idx),
                            Token::Semicolon,
                        ));
                    } else if c == '#' {
                        mode = Mode::Comment;
                    } else if c.is_alphabetic() {
                        mode = Mode::Word;
                        token_start = Location::new(line_idx, char_idx);
                        buffer.clear();
                    } else if c == '.' {
                        mode = Mode::Number;
                        token_start = Location::new(line_idx, char_idx);
                        buffer.clear();
                    } else if c.is_numeric() {
                        mode = Mode::Number;
                        token_start = Location::new(line_idx, char_idx);
                        buffer.clear();
                    } else if !c.is_whitespace() {
                        let location = Location::new(line_idx, char_idx);
                        let hint = format!("No valid token starts with the character '{}'!", c);
                        return Err(Error::InvalidCharacter(location, hint));
                    }
                }
                Mode::Word => {
                    if !c.is_alphanumeric() && c != '_' {
                        match Keyword::get_keyword(&buffer) {
                            Some(keyword) => tokens.push(ParsedToken(
                                token_start,
                                Token::Keyword(keyword),
                            )),
                            None => tokens.push(ParsedToken(
                                token_start,
                                Token::Name(buffer.clone()),
                            ))
                        }
                        mode = Mode::None;
                        buffer.clear();
                        continue;
                    }
                }
                Mode::Number => {
                    if c.is_alphabetic() {
                        let location = Location::new(line_idx, char_idx);
                        let hint = String::from("A number can only contain digits and a decimal separator!");
                        return Err(Error::InvalidCharacter(location, hint));
                    } else if c == '.' && buffer.contains('.') {
                        let location = Location::new(line_idx, char_idx);
                        let hint = String::from("A number can only contain one decimal separator!");
                        return Err(Error::InvalidCharacter(location, hint));
                    } else if !c.is_numeric() && c != '.' {
                        match buffer.parse() {
                            Ok(number) => {
                                tokens.push(ParsedToken(
                                    token_start,
                                    Token::Number(number),
                                ));
                                mode = Mode::None;
                                buffer.clear();
                                continue;
                            }
                            Err(_) => {
                                let hint = format!("Could not parse '{}'", buffer);
                                return Err(Error::InvalidNumber(token_start, hint));
                            }
                        }
                    }
                }
                Mode::Comment => {
                    if c == '\n' || c == '#' {
                        mode = Mode::None;
                    }
                }
            }
            buffer.push(c);
            if c == '\n' {
                line_idx += 1;
                line_start = i + 1;
            }
            break;
        }
    }
    Ok(TokenStream::new(tokens))
}

fn parse(mut tokens: TokenStream) -> Result<Solver> {
    let mut inputs = HashMap::new();
    let mut outputs = HashMap::new();

    loop {
        if tokens.empty() {
            break;
        }

        let name_location = tokens.location();
        let name = tokens.consume_name()?;
        tokens.consume_keyword(Keyword::Is)?;

        let set_location = tokens.location();
        if let Ok(_) = tokens.consume_keyword(Keyword::Between) {
            let min = tokens.consume_number()?;
            tokens.consume_keyword(Keyword::And)?;
            let max = tokens.consume_number()?;

            if let Ok(_) = tokens.consume_token(Token::Semicolon) {
                inputs.insert(name, Input::new(min, max));
            } else if let Ok(_) = tokens.consume_keyword(Keyword::Aggregated) {
                tokens.consume_keyword(Keyword::With)?;
                let agg_method = tokens.consume_aggregation_method()?;
                tokens.consume_keyword(Keyword::Defuzzified)?;
                tokens.consume_keyword(Keyword::With)?;
                let def_method = tokens.consume_defuzzification_method()?;
                tokens.consume_token(Token::Semicolon)?;
                outputs.insert(name, Output::new(min, max, agg_method, def_method));
            }
        } else if let Ok(set) = tokens.consume_name() {
            tokens.consume_keyword(Keyword::For)?;
            let function = tokens.consume_membership_function()?;
            if let Ok(_) = tokens.consume_token(Token::Semicolon) {
                let input = inputs.get_mut(&name)
                    .ok_or_else(|| Error::UndeclaredVariable(name_location, name))?;
                input.add_set(set, function)
                    .map_err(|err| {
                        match err {
                            crate::error::Error::DuplicateSetName(name) =>
                                Error::DuplicateSetName(set_location, name),
                            _ => unreachable!(),
                        }
                    })?;
            } else if let Ok(_) = tokens.consume_keyword(Keyword::When) {
                let rule = tokens.consume_rule()?;
                tokens.consume_keyword(Keyword::Implied)?;
                tokens.consume_keyword(Keyword::With)?;
                let imp_method = tokens.consume_implication_method()?;
                tokens.consume_token(Token::Semicolon)?;
                let output = outputs.get_mut(&name)
                    .ok_or_else(|| Error::UndeclaredVariable(name_location, name))?;
                output.add_set(set, OutputSet::new(function, rule, imp_method))
                    .map_err(|err| {
                        match err {
                            crate::error::Error::DuplicateSetName(name) =>
                                Error::DuplicateSetName(set_location, name),
                            _ => unreachable!(),
                        }
                    })?;
            } else {
                let expected = format!("Expected {} or {}", Keyword::When, Token::Semicolon);
                match tokens.next() {
                    Some(ParsedToken(location, token)) => {
                        let hint = format!("{}, got {}!", expected, token);
                        return Err(Error::InvalidToken(*location, hint));
                    }
                    None => {
                        let hint = format!("{}!", expected);
                        return Err(Error::UnexpectedEnd(hint));
                    }
                }
            }
        } else {
            match tokens.next() {
                Some(ParsedToken(location, token)) => {
                    let hint = format!("Expected {} or name, got {}!", Keyword::Between, token);
                    return Err(Error::InvalidToken(*location, hint));
                }
                None => {
                    let hint = format!("Expected {} or name!", Keyword::Between);
                    return Err(Error::UnexpectedEnd(hint));
                }
            }
        }
    }

    let solver = Solver::new(inputs, outputs);
    Ok(solver)
}

pub fn fz_to_slover(data: &str) -> Result<Solver> {
    parse(tokenize(data)?)
}
